"""Test fixtures"""
import asyncio
from pathlib import Path
import logging

import tomlkit  # type: ignore
import pytest
from datastreamcorelib.testhelpers import nice_tmpdir  # pylint: disable=W0611
from datastreamcorelib.logging import init_logging

from batterychecker.defaultconfig import DEFAULT_CONFIG_STR
from batterychecker.service import BatteryCheckerService


# pylint: disable=W0621
init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)


@pytest.fixture
@pytest.mark.asyncio
async def service_instance(nice_tmpdir):  # type: ignore
    """Create a service instance for use with tests"""
    parsed = tomlkit.parse(DEFAULT_CONFIG_STR)
    # Do not bind to TCP socket for testing and use test specific temp directory
    parsed["zmq"]["pub_sockets"] = ["ipc://" + str(Path(nice_tmpdir) / "batterychecker_pub.sock")]
    # Write a testing config file
    configpath = Path(nice_tmpdir) / "batterychecker_testing.toml"
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        fpntr.write(tomlkit.dumps(parsed))
    # Instantiate service and return it
    serv = BatteryCheckerService(configpath)
    return serv


@pytest.fixture
@pytest.mark.asyncio
async def running_service_instance(service_instance):  # type: ignore
    """Yield a running service instance, shut it down after the test"""
    task = asyncio.create_task(service_instance.run())
    # Yield a moment so setup can do it's thing
    await asyncio.sleep(0.1)

    yield service_instance

    service_instance.quit()

    try:
        await asyncio.wait_for(task, timeout=2)
    except TimeoutError:
        task.cancel()
    finally:
        # Clear alarms and default exception handlers
        BatteryCheckerService.clear_exit_alarm()
        asyncio.get_event_loop().set_exception_handler(None)
