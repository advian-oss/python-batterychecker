"""Default configuration file contents"""

# Remember to add tests for keys into test_batterychecker.py
DEFAULT_CONFIG_STR = """
[zmq]
pub_sockets = ["ipc:///tmp/batterychecker_pub.sock", "tcp://*:63486"]

[gpib]
port = "/dev/ttyUSB0"
port_args = {}
address = 5

[test]
load = 0.5 # Amperes
stop_loaded = 3.275 # Volts, stop test if loaded voltage drops below this limit
stop_unloaded = 3.550 # Volts, stop test if unloaded voltage drops below this limit

""".lstrip()
