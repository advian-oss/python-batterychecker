"""Main classes for batterychecker"""
from typing import Optional
import asyncio
from dataclasses import dataclass, field
import logging
import pendulum


from datastreamservicelib.service import SimpleService
from datastreamcorelib.datamessage import PubSubDataMessage
from scpi.transports.gpib import prologix
from scpi.errors import CommandError
from scpi.devices.hp6632b import HP6632B

LOGGER = logging.getLogger(__name__)


@dataclass
class BatteryCheckerService(SimpleService):
    """Main class for batterychecker"""

    gpib: Optional[prologix.PrologixGPIBTransport] = field(default=None)

    async def teardown(self) -> None:
        """tearodwn"""
        await self.stop_named_task_graceful("testloop")
        if self.gpib:
            LOGGER.debug("quitting GPIB transport")
            await self.gpib.quit()
        # reset the alarm
        BatteryCheckerService.set_exit_alarm()
        await super().teardown()

    def reload(self) -> None:
        """Load configs, restart sockets"""
        super().reload()
        self.create_task(self.async_reload())

    async def async_reload(self) -> None:
        """Async reloads"""
        if self.gpib:
            await self.gpib.quit()
        self.gpib = prologix.get(self.config["gpib"]["port"], **self.config["gpib"]["port_args"])
        self.create_task(self.testloop_task(), name="testloop")

    async def testloop_task(self) -> None:  # pylint: disable=R0915
        """Init and read the multimeter"""
        if self.gpib is None:
            raise RuntimeError("no transport")
        dtransport = self.gpib.get_device_transport(self.config["gpib"]["address"])
        device = HP6632B(dtransport)
        await device.set_output(False)
        await device.set_low_current_mode(False)  # we're not working below 20mA
        await device.set_voltage(0)  # We are operating as load
        await device.set_current(int(self.config["test"]["load"] * 1000))
        first_measurement = True
        first_msg: Optional[PubSubDataMessage] = None
        last_msg: Optional[PubSubDataMessage] = None
        try:
            while self.psmgr.default_pub_socket and not self.psmgr.default_pub_socket.closed and self._exitcode is None:
                try:
                    await device.set_output(True)
                    await asyncio.sleep(0.1)
                    loaded_volts = await device.measure_voltage()
                    loaded_current = await device.measure_current()
                    await device.set_output(False)
                    await asyncio.sleep(0.1)
                    unloaded_volts = await device.measure_voltage()
                except CommandError:
                    LOGGER.exception("Got a problem")
                    continue
                msg = PubSubDataMessage(b"battery_stats")
                msg.data.update(
                    {
                        "unloaded_volts": float(unloaded_volts),
                        "loaded_volts": float(loaded_volts),
                        "loaded_current": float(loaded_current),
                        "set_current": self.config["test"]["load"],
                        "last_measurement": False,
                        "first_measurement": first_measurement,
                    }
                )
                # Check the limits
                if loaded_volts < self.config["test"]["stop_loaded"]:
                    LOGGER.info("Loaded voltage limit reached, stopping test")
                    msg.data["last_measurement"] = True
                if unloaded_volts < self.config["test"]["stop_unloaded"]:
                    LOGGER.info("Loaded voltage limit reached, stopping test")
                    msg.data["last_measurement"] = True

                LOGGER.debug("PUBlishing {}".format(msg))
                await self.psmgr.publish_async(msg)
                if msg.data["first_measurement"]:
                    first_msg = msg
                if msg.data["last_measurement"]:
                    last_msg = msg
                first_measurement = False

                # Quit and break if we're done
                if msg.data["last_measurement"]:
                    await device.set_output(False)
                    self.quit(0)
                    break
        except asyncio.CancelledError:
            LOGGER.debug("cancelled")
        if first_msg and last_msg:
            start_dt = pendulum.parse(first_msg.data["systemtime"])  # type: ignore
            end_dt = pendulum.parse(last_msg.data["systemtime"])  # type: ignore
            took = end_dt - start_dt
            LOGGER.info(
                "Test took {}, start voltage {:.3f} ({:.3f} loaded), end voltage {:.3f} ({:.3f} loaded), set current was {:.3f}".format(  # pylint: disable=C0301
                    took,
                    first_msg.data["unloaded_volts"],
                    first_msg.data["loaded_volts"],
                    last_msg.data["unloaded_volts"],
                    last_msg.data["loaded_volts"],
                    self.config["test"]["load"],
                )
            )

        LOGGER.debug("task done")
