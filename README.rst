==============
batterychecker
==============

Use SCPI controlled programmable load to check battery capacity, currently assumes HP6632B and Prologix USB-GPIB controller

Combine with https://gitlab.com/advian-oss/python-dsinfluxlogger to get nice graphs.

.. image:: ./batterychecker_grafana.png
  :width: 800
  :alt: Two graphs of battery voltage over time

Docker
------

For more controlled deployments and to get rid of "works on my computer" -syndrome, we always
make sure our software works under docker.

It's also a quick way to get started with a standard development environment.

Creating a development container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Build image, create container and start it::

    docker build --ssh default --target devel_shell -t batterychecker:devel_shell .
    docker create --name batterychecker_devel --device /dev/ttyUSB0:/dev/ttyUSB0 -p 63486:63486 -v `pwd`":/app" -it -v /tmp:/tmp batterychecker:devel_shell
    docker start -i batterychecker_devel

pre-commit considerations
^^^^^^^^^^^^^^^^^^^^^^^^^

If working in Docker instead of native env you need to run the pre-commit checks in docker too::

    docker exec -i batterychecker_devel /bin/bash -c "pre-commit install"
    docker exec -i batterychecker_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use the docker run syntax but using
the running container is faster::

    docker run -it --rm -v `pwd`":/app" batterychecker:devel_shell -c "pre-commit run --all-files"

Test suite
^^^^^^^^^^

You can use the devel shell to run py.test when doing development, for CI use
the "tox" target in the Dockerfile::

    docker build --ssh default --target tox -t batterychecker:tox .
    docker run -it --rm -v `pwd`":/app" batterychecker:tox

Production docker
^^^^^^^^^^^^^^^^^

There's a "production" target as well for running the application (change "myconfig.toml" for
config file)::

    docker build --ssh default --target production -t batterychecker:latest .
    docker run -it --name batterychecker --device /dev/ttyUSB0:/dev/ttyUSB0 -v myconfig.toml:/app/config.toml -p 63486:63486 -it -v /tmp:/tmp batterychecker:latest


Local Development
-----------------

TLDR:

- Create and activate a Python 3.8 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.8` my_virtualenv

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    pre-commit install
    pre-commit run --all-files

- Ready to go, try the following::

    batterychecker --defaultconfig >config.toml
    batterychecker -vv config.toml

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).

Running "pre-commit run --all-files" and "py.test -v" regularly during development and
especially before committing will save you some headache.
